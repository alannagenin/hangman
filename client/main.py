import requests
from dotenv import load_dotenv
import os


def get_mots():
    req = requests.get(os.getenv(URL) + str("mots"))
    return req.json()


if __name__ == "__main__":
    load_dotenv()
