import requests
from fastapi import FastAPI
from mot import Mot

app = FastAPI()


@app.get("/")
def read_root():
    return "Mot mot motus !"


@app.get("/mots")
def get_mots():
    return {"mots": Mot.getMots()}


@app.post("/mot")
def add_mot(word: Mot):
    print(word.caracteres)
    return word
