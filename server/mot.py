from pydantic import BaseModel


class Mot(BaseModel):
    id: int
    caracteres: str

    # def __init__(self, id, caracteres):
    #   self.id = id
    #   self.caracteres = caracteres

    def getMots():
        known_words = {"words": [Mot(id=1, caracteres="banane"),
                                 Mot(id=2, caracteres="poire"),
                                 Mot(id=3, caracteres="pomme"),
                                 Mot(id=4, caracteres="fraise"),
                                 Mot(id=5, caracteres="kiwi")
                                 ]
                       }
        words = list()
        for word in known_words["words"]:
            words.append(word.caracteres)
        return words

    def addMot(mot):
        return mot
