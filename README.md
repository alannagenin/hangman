# TP6 - Conception logicielle

Ce projet a pour but de créer un pendu en faisant appel à un modèle client-serveur.

## Initialisation du projet

Pour utiliser ce service, vous devez cloner le repo et installer les dépendances.

```
git clone git@gitlab.com:alannagenin/hangman.git
cd hangman
pip install -r requirements.txt
```

# Lancer l'application

Ensuite vous pourrez vous amuser à requêter des mots et ajouter d'autres.

## Lancer le module

```
cd server
uvicorn main:app --reload
```

## Requêter le serveur [^1]

Lancer les commandes suivantes dans un autre terminal :

```
cd ../client
python3 main.py
```
